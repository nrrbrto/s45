const coursesData = 
[   {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?",
        price: 45000,
        onOffer: true
    },

    {
        id: "wdc002",
        name: "Python - Django",
        description: "  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?",
        price: 50000,
        onOffer: true
    },

    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?",
        price: 55000,
        onOffer: true
    }
]
    export default coursesData;
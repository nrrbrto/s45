import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App />
);

//JSX is a syntax used in Reactjs
//able to create HTML elements using JS
//index.js is the main entry point, it imports App.js, binding it to root in index.html
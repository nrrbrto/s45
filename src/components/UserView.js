import React, {useState, useEffect} from "react"
import CourseCard from './CourseCard'

export default function UserView({coursesData}){ //nasa curly braces kasi un[packed]
    const [courses, setCourses] = useState();

    useEffect(() =>{
        
        const coursesArr = coursesData.map(course => {
            //only active courses
            if (course.isActive === true) {
                return (
                    <CourseCard courseProp={course} key={course.id}/>
                )
            } else {
                return null
            }
        })
        setCourses(coursesArr)
    }, [coursesData])
    

    return (
        <>
            {courses}
        </>
    )
}
import React, { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from '../UserContext'

export default function Register() {

    const {user} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [pass1, setPass1] = useState('');
    const [pass2, setPass2] = useState('');
    
    const [isActive, setIsActive] = useState(true)

    useEffect (() => {
        if ((email!=='' && pass1 !== '' && pass2 !== '')&&(pass1 === pass2)){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, pass1, pass2])

    function registerUser(e) {
        e.preventDefault();
        setEmail('')
        setPass1('')
        setPass2('')

        Swal.fire({
            title: "Yes!",
            icon: "success",
            text: "You have successfully registered"
        }) 
    }

    return (
        //conditional rendering
        (user.acessToken !== null ) ?
        <Navigate to="/" />
        :

        <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group>
            <Form.Label>Email Address</Form.Label>
            <Form.Control 
                type="email"
                placeholder="Enter Email"
                required
                value={email}  
                onChange={e => setEmail(e.target.value)}  
                />
            <Form.Text className="text-muted">
                We'll never share your email with anyone else
            </Form.Text>
        </Form.Group>

        <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password"
                placeholder="Enter Password"
                required
                value={pass1}
                onChange={e => setPass1(e.target.value)}  
                />
        </Form.Group>

        <Form.Group>
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
                type="password"
                placeholder="Verify Password"
                required
                value={pass2}
                onChange={e => setPass2(e.target.value)}  
                />
        </Form.Group>
        <Button variant="primary" type="submit" disabled= {isActive===false} >Submit</Button>
            
        </Form>
    )
}
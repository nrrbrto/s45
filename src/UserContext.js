import React from "react";

const UserContext = React.createContext();

//creates a context objecy
//a context object is a data type of an object that can be used to store info that can be shared to other components within the app


export const UserProvider = UserContext.Provider;

export default UserContext;
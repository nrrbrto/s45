import React, { useState } from 'react';
import './App.css';
import { Container } from 'react-bootstrap';

import AppNavbar from './components/AppNavbar'
import Error from './components/Error';


import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificCourse from './pages/SpecificCourse';

import { UserProvider } from './UserContext'

//for routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

function App() {

  // React context is nothing but a global state to the app. It's a way to make particular data available to all the components kahit greatgrand child

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/courses' element={<Courses />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path="/courses/:courseId" element={<SpecificCourse />} />
            <Route path='*' element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  );
}


export default App;
